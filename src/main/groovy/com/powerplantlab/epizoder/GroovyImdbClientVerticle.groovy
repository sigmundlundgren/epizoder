package com.powerplantlab.epizoder
import groovy.json.JsonSlurper
import org.vertx.groovy.core.buffer.Buffer
import org.vertx.groovy.platform.Verticle
/*
 * Imdb TV API Client Verticle
 *
 * @author Sigmund Lundgren
 */

class GroovyImdbClientVerticle extends Verticle {


    def start() {
        container.logger.info "Starting ImdbCLientVerticle"
        def host = container.config."imdb-api-uri"
        def port = container.config."imdb-api-port"
        def httpClient = vertx.createHttpClient(host: host, port: port, keepAlive: true, maxPoolSize: 5)

        container.logger.info "IMDB TV API URI ${host}:${port}"

        vertx.eventBus.registerHandler("get.tvshow.by.name") { message ->
            getTVShowByName(httpClient, message)
        }

        vertx.eventBus.registerHandler("get.tvshow.by.id") { message ->
            getTVShowByID(httpClient, message)
        }

        vertx.eventBus.registerHandler("get.tvshow.season") { message ->
            getTVShowSeason(httpClient, message)
        }

        container.logger.info "Started ImdbCLientVerticle"
    }

    private def getTVShowByName(httpClient, message) {
        container.logger.info "Getting TV show by name ${message.body}"
        def path = container.config.'imdb-tv-search-api-path'
        def api_key = container.config.'imdb-api-key'
        def api_version = container.config.'imdb-api-version'

        def url = "/${api_version}/${path}?api_key=${api_key}&query=${URLEncoder.encode(message.body as String, 'UTF-8')}"
        container.logger.info "Connecting to ${url}"

        httpClient.getNow(url) { response ->
            container.logger.info "got response: ${response.statusCode} ${response.statusMessage}"
            if(isErrorResponse(response)) {
                handleErrorResponse(response, message)
                return
            }
            def body = getResponseBody(response)
            response.endHandler {
                def json = new JsonSlurper().parseText(body as String)
                def tv_show = json.results[0]
                message.reply([status: "OK", result: tv_show])
            }
        }
        .exceptionHandler { e ->
            container.logger.warn(e)
            message.reply([status: "ERROR", result: [e]])
        }
    }

    private def getTVShowByID(httpClient, message) {
        container.logger.info "Getting TV show by ID ${message.body}"
        def path = container.config.'imdb-tv-api-path'
        def api_key = container.config.'imdb-api-key'
        def api_version = container.config.'imdb-api-version'
        def tv_show_id = message.body
        def url = "/${api_version}/${path}/${tv_show_id}?api_key=${api_key}"
        container.logger.info "Connecting to ${url}"

        httpClient.getNow(url) { response ->
            container.logger.info "got response: ${response.statusCode} ${response.statusMessage}"
            if(isErrorResponse(response)) {
                handleErrorResponse(response, message)
                return
            }
            def body = getResponseBody(response)
            response.endHandler {
                def json = new JsonSlurper().parseText(body as String)
                message.reply([status: "OK", result: json])
            }
        }
        .exceptionHandler { e ->
            container.logger.warn(e)
            message.reply([status: "ERROR", result: [e]])
        }
    }

    private def getTVShowSeason(httpClient, message) {
        container.logger.info "Getting TV show season ${message.body}"
        def path = container.config.'imdb-tv-api-path'
        def api_key = container.config.'imdb-api-key'
        def api_version = container.config.'imdb-api-version'
        def tv_show_id = message.body.id
        def url = "/${api_version}/${path}/${tv_show_id}/season/${message.body.season}?api_key=${api_key}"
        container.logger.info "Connecting to ${url}"

        httpClient.getNow(url) { response ->
            container.logger.info "got response: ${response.statusCode} ${response.statusMessage}"
            if(isErrorResponse(response)) {
                handleErrorResponse(response, message)
                return
            }
            def body = getResponseBody(response)

            response.endHandler {
                def json = new JsonSlurper().parseText(body as String)
                message.reply([status: "OK", result: json])
            }
        }
        .exceptionHandler { e ->
            container.logger.warn(e)
            message.reply([status: "ERROR", result: [e]])
        }
    }


    private static def getResponseBody(response) {
        def body = new Buffer()
        response.dataHandler { buffer ->
            body << buffer
        }
        body
    }

    private static def isErrorResponse(response) {
        return response.statusCode != 200
    }

    private def handleErrorResponse(response, message) {
        container.logger.warn "Error ${response.statusCode} ${response.statusMessage}"
        message.reply([status: "ERROR", result: "Error ${response.statusCode} ${response.statusMessage}"])
    }

}
