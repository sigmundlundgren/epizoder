package integration_tests.groovy
import com.powerplantlab.epizoder.GroovyImdbClientVerticle
import io.vertx.rxcore.java.eventbus.RxEventBus
import org.vertx.groovy.testtools.VertxTests
import rx.util.functions.Action1

import static org.vertx.testtools.VertxAssert.*

config = [
        "imdb-api-uri"           : "api.themoviedb.org",
        "imdb-movie-api-path"    : "search/movie",
        "imdb-tv-search-api-path": "search/tv",
        "imdb-tv-api-path"       : "tv",
        "imdb-api-version"       : "3",
        "imdb-api-port"          : 80,
        "imdb-api-key"           : "bd3321a38128789304ead15584f2d30a"
]

def testGetTvShows() {
    container.deployVerticle("groovy:" + GroovyImdbClientVerticle.class.getName(), config) { asyncResult ->

        if (!asyncResult.succeeded) {
            fail(asyncResult.cause)
            return
        }

        RxEventBus rxEventBus = new RxEventBus(vertx.eventBus.javaEventBus())

        println "The verticle has been deployed, deployment ID is $asyncResult.result"


        def obs = rxEventBus.send("get.tvshow.by.name", "Banshee")

        obs.subscribe(
                new Action1() {
                    public void call(message) {
                        assertEquals("OK", message.body().getString("status"))
                        println "Result ${message.body().getField('result')}"
                        testComplete()
                    }
                }
        )


        if (false) {
            vertx.eventBus.send("get.tvshow.by.name", "Banshee", { reply ->
                assertEquals("OK", reply.body.status)
                println "Result $reply.body.result"

                def tv_show_id = reply.body.result.id
                assertEquals(41727, tv_show_id)

                vertx.eventBus.send("get.tvshow.by.id", tv_show_id) { show_reply ->
                    assertEquals("OK", show_reply.body.status)
                    println "Result $show_reply.body.result"

                    def number_of_seasons = show_reply.body.result."number_of_seasons"
                    println "Number of episodes $number_of_seasons"

                    for (i = 1; i <= number_of_seasons; i++) {
                        vertx.eventBus.send("get.tvshow.season", [id: tv_show_id, season: i]) { season_reply ->
                            assertEquals("OK", season_reply.body.status)
                            println "Result $season_reply.body.result"
                        }

                        vertx.setTimer(2000, { timerID ->
                            assertNotNull(timerID)

                            // This demonstrates how tests are asynchronous - the timer does not fire until 1 second later -
                            // which is almost certainly after the test method has completed.
                            testComplete()
                        })
                    }
                }

            })
        }
    }
}


VertxTests.initialize(this)
VertxTests.startTests(this)


